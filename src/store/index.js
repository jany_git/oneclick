import { createStore } from 'redux';
import reducers from './../reducers';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const state = {
    answers: {},
    test: {validated: false}
}
export const store = createStore(reducers, state,  composeEnhancers);