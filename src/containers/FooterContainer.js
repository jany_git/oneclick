import React from 'react';
import PropTypes from 'prop-types';
import Footer from '../components/Footer';
import { connect } from "react-redux";
import { showFormError, deleteFormError } from './../actions/errors';
import { deleteAllAnswers } from './../actions/answers';
import { setTestValidated } from './../actions/test';

const FooterContainer = (props) => <Footer {...props}/>

FooterContainer.propTypes = {
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired,
    showFormError: PropTypes.func.isRequired,
    deleteFormError: PropTypes.func.isRequired,
    setTestValidated: PropTypes.func.isRequired,
    deleteAllAnswers: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    answers: state.answers,
    testValidated: state.test.validated
})

export default connect(mapStateToProps, {showFormError, deleteFormError, setTestValidated, deleteAllAnswers})(FooterContainer);