import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormError from '../components/FormError';

const FormErrorContainer = (props) => <FormError {...props} />;

FormErrorContainer.propTypes = {
    formError: PropTypes.bool
};

const mapStateToProps = (state) => ({
    formError: state.errors.formError
});

export default connect(mapStateToProps, null)(FormErrorContainer);