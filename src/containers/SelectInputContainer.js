import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SelectInput from '../components/SelectInput';
import { setAnswer } from './../actions/answers';

const SelectInputContainer = (props) => <SelectInput {...props} ></SelectInput>

SelectInputContainer.propTypes = {
    options: PropTypes.array.isRequired,
    index: PropTypes.number.isRequired,
    idQuestion: PropTypes.string.isRequired,
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired
};

const mapStateToProps = (state, props) => ({
    answers: state.answers,
    testValidated: state.test.validated
})

export default connect(mapStateToProps, {setAnswer})(SelectInputContainer);