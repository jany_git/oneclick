import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TemplateClozedropdown from '../components/TemplateClozedropdown';

const TemplateClozedropdownContainer = (props) => <TemplateClozedropdown {...props} />

TemplateClozedropdownContainer.propTypes = {
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
    answers: state.answers,
    testValidated: state.test.validated
})

export default connect(mapStateToProps, null)(TemplateClozedropdownContainer);