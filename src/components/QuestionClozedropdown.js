import React from 'react';
import PropTypes from 'prop-types';
import Stimulus from './Stimulus';
import TemplateClozedropdownContainer from './../containers/TemplateClozedropdownContainer';

const QuestionClozedropdown = ({question}) => {
    return (
        <div>
            <Stimulus text={question.data.stimulus}/>
            <div>
                <TemplateClozedropdownContainer question={question}></TemplateClozedropdownContainer>
            </div>
        </div>
    );
};

QuestionClozedropdown.propTypes = {
    question: PropTypes.object.isRequired
};

export default QuestionClozedropdown;