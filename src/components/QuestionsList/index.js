import React from 'react';
import { QUESTIONS } from '../../constants/questions';
import QuestionItem from '../QuestionItem';
import FormErrorContainer from '../../containers/FormErrorContainer';
import './styles.css';

const renderQuestions = (questions) => {
    return questions.map(question => {
        return <QuestionItem key={question.reference} question={question}/>
    })
}

const QuestionsList = props => {
    return (
        <div className="oc-questions-list">
            {renderQuestions(QUESTIONS)}
            <FormErrorContainer />
        </div>
    );
};

export default QuestionsList;