import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { getIsCompletedTest } from '../../helpers/test';
import { QUESTIONS } from '../../constants/questions';
import ReviewStateTest from '../ReviewStateTest';
import './styles.css';
import * as Scroll from 'react-scroll';
var scroll = Scroll.animateScroll;

class Footer extends Component {

    _correctTest = () => {
        this.props.setTestValidated(true);
    }

    // validamos que estén todas las preguntas correctas, si lo están lo corregimos, 
    // si no, mostramos error.
    handleValidate = () => {
        if (getIsCompletedTest(this.props.answers, QUESTIONS)) {
            this.props.deleteFormError();
            this._correctTest()
        } else {
            scroll.scrollToBottom();
            this.props.showFormError();
        }
    }

    handleReset = () => {
        this.props.setTestValidated(false);
        this.props.deleteAllAnswers();
    }

    //  si está el test validado, se pinta el boton de reiniciar y  el resumen, 
    //  si no, solo el botón de validadas.
    _renderButtonsAndDetails = () => {
        if (this.props.testValidated) {
            return (
                <Fragment>
                    <button className="oc-button" onClick={this.handleReset}> Reiniciar </button>
                    <ReviewStateTest questions={QUESTIONS} answers={this.props.answers} />
                </Fragment>
            )
        } else {
            return <button className="oc-button" onClick={this.handleValidate}> Comprobar respuestas </button>
        }
    }

    render() {
        return (
            <div className="oc-footer">
                {this._renderButtonsAndDetails()}
            </div>
        );
    }
};

Footer.propTypes = {
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired,
    showFormError: PropTypes.func.isRequired,
    deleteFormError: PropTypes.func.isRequired,
    setTestValidated: PropTypes.func.isRequired,
    deleteAllAnswers: PropTypes.func.isRequired
};


export default Footer;