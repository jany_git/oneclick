import React from 'react';
import PropTypes from 'prop-types';

const createMarkup = (text) => ({__html: text});

const InnerHTML = ({text}) => <div className="oc-d-inline" dangerouslySetInnerHTML={createMarkup(text)}></div>;

InnerHTML.propTypes = {
    text: PropTypes.string.isRequired
};

export default InnerHTML;