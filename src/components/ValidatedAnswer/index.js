import React from 'react';
import PropTypes from 'prop-types';
import { IoIosCheckmark, IoIosClose } from "react-icons/io";
import { getIsCorrectAnswer } from '../../helpers/test';
import './styles.css';

const getClass = (isCorrectAnswer) => (isCorrectAnswer ? 'oc-correct-answer' : 'oc-incorrect-answer');

const ValidatedAnswer = props => {
    const {index, answers, question} = props;
    const isCorrectAnswer = getIsCorrectAnswer(index, answers, question);
    return (
        <div className={`oc-d-inline-block oc-answer ${getClass(isCorrectAnswer)}`}>
            <div className="oc-index"> {index+1} </div>
            <div className="oc-text">
                <span className="only-small-screen">{index+1}. </span> 
                {answers[question.reference][index].optionSelected}
            </div>
            <div className="oc-icon">{isCorrectAnswer ? <IoIosCheckmark/> : <IoIosClose/>}</div>
        </div>
    );
};

ValidatedAnswer.propTypes = {
    index: PropTypes.number.isRequired,
    answers: PropTypes.object.isRequired,
    question: PropTypes.object.isRequired,
};

export default ValidatedAnswer;
