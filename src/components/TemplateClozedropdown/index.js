import React, {Fragment, Component} from 'react';
import PropTypes from 'prop-types';
import { IoIosCheckmark, IoIosClose } from "react-icons/io";
import InnerHTML from '../InnerHTML';
import SelectInputContainer from '../../containers/SelectInputContainer';
import ValidatedAnswer from '../ValidatedAnswer';
import CorrectAnswerList from '../CorrectAnswerList';
import { splitByString, isLastItem } from '../../helpers/array';
import { getIsCorrectQuestion } from '../../helpers/test';
import './styles.css';

class TemplateClozedropdown extends Component {
    putTextAndSelects = (question) => {
        const {template, possible_responses} = question.data;
        const partTexts = splitByString(template, '{{response}}'); // lo divido por donde hay que añadir el combo.
        const partTextsLength = partTexts.length;
    
        return partTexts.map((item, index) => {
            //  si el test está validado pintamos la respuesta, si no, el combo.
            const componentSelectOrValidatedAnswer = this.props.testValidated ?
            <ValidatedAnswer index={index} answers={this.props.answers} question={question}></ValidatedAnswer> : 
            <SelectInputContainer options={possible_responses} index={index} idQuestion={question.reference}></SelectInputContainer>
            return (isLastItem(partTextsLength, index) ? // si es la ultima parte no hay que añadir el combo/respuesta, si no, sí.
                <Fragment key={index}><InnerHTML text={item}></InnerHTML>{componentSelectOrValidatedAnswer}</Fragment> : 
                <InnerHTML key={index} text={item}></InnerHTML>);
        })
    }

    getClass = (correctQuestion) => (correctQuestion ? 'oc-correct-template' : 'oc-incorrect-template');

    renderCorrectAnswers = () => (<CorrectAnswerList answers={this.props.answers} question={this.props.question}/>)

    renderIcon = (isCorrect, classForTemplate) => <div className={`oc-icon-template ${classForTemplate}`}>{isCorrect ? <IoIosCheckmark/> : <IoIosClose/>}</div>

    render() {
        const {testValidated, answers, question} = this.props;
        const isCorrect =  testValidated ? getIsCorrectQuestion(answers, question) : null;
        const classForTemplate = testValidated ? this.getClass(isCorrect) : '';
        return (
            <Fragment>
                {testValidated ? this.renderIcon(isCorrect, classForTemplate) : null}
                <div className={`oc-template ${classForTemplate}`}> 
                    {this.putTextAndSelects(question)}
                </div>
                {testValidated ? this.renderCorrectAnswers() : null}
            </Fragment> 
        );
    }
 
};

TemplateClozedropdown.propTypes = {
    question: PropTypes.object.isRequired,
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired
};

export default TemplateClozedropdown;