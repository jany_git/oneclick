import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

class FormError extends Component {

    // si hay error lo mostramos
    renderError = () => (this.props.formError) ? 
        <div className="oc-div-error">Debes rellenar todos los combos para validar.</div> : null

    render() {
        return (
            <Fragment>
                {this.renderError()}
            </Fragment>
        );
    }
}

FormError.propTypes = {
    formError: PropTypes.bool
};

export default FormError;