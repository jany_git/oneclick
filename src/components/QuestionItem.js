import React from 'react';
import PropTypes from 'prop-types';
import QuestionClozedropdown from './QuestionClozedropdown';

const renderTypeQuestion = (question) => {
    if (question.type === 'clozedropdown') {
        return <QuestionClozedropdown question={question} />
    } else {
        return <div className="oc-div-error">El tipo de pregunta: {question.type} no esta soportada. </div>
    }
}

const QuestionItem = ({question}) => {
    return (
        <div>
           {renderTypeQuestion(question)}
        </div>
    );
};

QuestionItem.propTypes = {
    question: PropTypes.object.isRequired
};

export default QuestionItem;