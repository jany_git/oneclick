import React from 'react';
import PropTypes from 'prop-types';
import { getReviewTest } from './../../helpers/test';
import './styles.css';

const ReviewStateTest = props => {
    const dataReview = getReviewTest(props.answers, props.questions);
    return (
        <div className="oc-review-state">
            <div className="oc-review-state-info oc-d-inline-block">
                {dataReview.correctQuestions} / {dataReview.totalQuestions}
            </div>
            <div className="oc-d-inline-block oc-review-state-text">Correctas</div>
        </div>
    );
};

ReviewStateTest.propTypes = {
    answers: PropTypes.object.isRequired,
    questions: PropTypes.array.isRequired,
};

export default ReviewStateTest;