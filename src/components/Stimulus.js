import React from 'react';
import PropTypes from 'prop-types';
import InnerHTML from './InnerHTML';

const Stimulus = ({text}) => {
    return (
        <InnerHTML text={text} />
    );
};

Stimulus.propTypes = {
    text: PropTypes.string.isRequired
};

export default Stimulus;