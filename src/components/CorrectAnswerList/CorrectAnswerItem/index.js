import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const CorrectAnswerItem = props => {

    const {index, text} = props; 
    const indexToShow = index+1;
    
    return (
        <div className="oc-correct-answer-item oc-d-inline-block">
            <div className="oc-correct-answer-index-container oc-d-inline-block">{indexToShow}</div>
            <div className="oc-correct-answer-text-container oc-d-inline-block">
                <span className="only-small-screen">{indexToShow}.</span>{text}
            </div>
        </div>
    );
};

CorrectAnswerItem.propTypes = {
    text: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
};

export default CorrectAnswerItem;