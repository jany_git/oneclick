import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {getIncorrectAnswers} from './../../helpers/test' 
import CorrectAnswerItem from './CorrectAnswerItem';
import './styles.css';

class CorrectAnswerList extends Component {

    _renderItems = (incorrectAnswers) => {
        return incorrectAnswers.map(item => {
            return <CorrectAnswerItem key={item.text} index={item.index} text={item.text} />
        })
    }

    renderCorrectListItem = (answers, question) => {
        const incorrectAnswers = getIncorrectAnswers(answers, question);
        if (!incorrectAnswers || incorrectAnswers.length === 0) {
            return null;
        } else {
            return (<div className="oc-correct-list">
                <div>Respuestas Correctas:</div>
                {this._renderItems(incorrectAnswers)}
            </div>)
        } 
    }

    render() {
        const {answers, question} = this.props;
        return (
            <Fragment >
                {this.renderCorrectListItem(answers, question)}
            </Fragment>
        );
    }
}

CorrectAnswerList.propTypes = {
    answers: PropTypes.object.isRequired,
    question: PropTypes.object.isRequired
};

export default CorrectAnswerList;