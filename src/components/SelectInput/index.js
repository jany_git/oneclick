import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './styles.css';

class SelectInput extends Component {
    
    state = {
        valueSelect: ''
    }

    renderOptions = (options, index) => {
        const selectOptions = options[index].map(item => {
            return <option key={item}>{item}</option>
        })
        return [<option key='Selecciona una opción'>Selecciona una opción</option>, ...selectOptions];
    }

    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
          [name]: value
        });

        const answerData = {
            idQuestion: this.props.idQuestion,
            index: this.props.index,
            value: value,
        }

        this.props.setAnswer(answerData);
    }

    render() {
        return (
            <select value={this.state.valueSelect} onChange={this.handleInputChange} name='valueSelect' className="oc-select">
                {this.renderOptions(this.props.options, this.props.index)}
            </select>
        );
    };
};

SelectInput.propTypes = {
    options: PropTypes.array.isRequired,
    index: PropTypes.number.isRequired,
    idQuestion: PropTypes.string.isRequired,
    answers: PropTypes.object.isRequired,
    testValidated: PropTypes.bool.isRequired
};

export default SelectInput;