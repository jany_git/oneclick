import { combineReducers } from 'redux';
import { answers } from './answers';
import { errors } from './errors';
import { test } from './test';

export default combineReducers({
    answers,
    errors,
    test
});