import { handleActions} from 'redux-actions';
import {SET_ANSWER, DELETE_ALL_ANSWER} from './../constants/actions';

export const answers = handleActions({
    [SET_ANSWER]:  (state, action) => {
        const {idQuestion, index, value} = action.payload;
        if (value === 'Selecciona una opción') {
            const {[idQuestion]: current, ..._state} = state;
            const {[index]: removed, ...newData} = current || {};
            if (Object.keys(newData).length !== 0) {
                _state[idQuestion] = newData;
            }
            return _state;
        } else {
            const prev = state[idQuestion] || {};
            return {...state, [idQuestion]: {...prev, [index]: {optionSelected: value}}}
        }
    },
    [DELETE_ALL_ANSWER]:  (state, action) => {
        return {};
    }
    
}, [])
