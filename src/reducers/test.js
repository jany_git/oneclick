import { handleActions} from 'redux-actions';
import {TEST_VALIDATED} from './../constants/actions';

export const test = handleActions({
    [TEST_VALIDATED]:  (state, action) => {
        return {...state, validated: action.payload}
    }
}, [])
