import { handleActions} from 'redux-actions';
import {FORM_ERROR, DELETE_FORM_ERROR} from './../constants/actions';

export const errors = handleActions({
    [FORM_ERROR]:  (state, action) => {
        return {...state, formError: true}
    },

    [DELETE_FORM_ERROR]:  (state, action) => {
        return {...state, formError: false}
    }
}, [])
