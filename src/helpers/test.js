export const getIsCompletedTest = (answers, QUESTIONS) => {
    let sumOptions = 0;
    QUESTIONS.forEach(element => {
        sumOptions += element.data.possible_responses.length;
    });

    let sumAnswers = 0;
    Object.keys(answers).forEach(element => {
        sumAnswers += Object.keys(answers[element]).length 
    });

    return sumAnswers === sumOptions ? true : false;
};


export const getIsCorrectQuestion = (answers, question) => {
    let isCorrectQuestion = true;
    question.data.validation.valid_response.value.forEach((rightResponse, index) => {
        if (answers[question.reference][index].optionSelected !== rightResponse) {
            isCorrectQuestion = false;
        }
    })
    return isCorrectQuestion;
}

export const getIsCorrectAnswer = (index, answers, question) => {
    const optionSelected = answers[question.reference][index].optionSelected;
    let isCorrect = (question.data.validation.valid_response.value[index] === optionSelected) ? true : false;
    return isCorrect;
}

export const getReviewTest = (answers, QUESTIONS) => {
    const data = {
        totalQuestions: 0,
        correctQuestions: 0
    }

    QUESTIONS.forEach(element => {
        data.totalQuestions += 1;
        if (getIsCorrectQuestion(answers, element)) {
            data.correctQuestions += 1;
        }
    });

    return data;
}

export const getIncorrectAnswers = (answers, question) => {
    const incorrectQuestion = []; 
    let isCorrect, newIncorrectAnswer;
    question.data.validation.valid_response.value.forEach((textCorrectAnswer, index) => {
        isCorrect = getIsCorrectAnswer(index, answers, question);
        if (!isCorrect) {
            newIncorrectAnswer = {
                text: textCorrectAnswer,
                index
            } 
            incorrectQuestion.push(newIncorrectAnswer)
        } 
    })

    return incorrectQuestion;

}