export const splitByString = (template, textToSplit) => template.split(textToSplit);
export const isLastItem = (responsesLength, index) => (responsesLength !== index + 1) 
