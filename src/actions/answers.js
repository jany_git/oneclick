import {createAction} from 'redux-actions';
import {SET_ANSWER, DELETE_ALL_ANSWER} from './../constants/actions';

export const setAnswer = createAction(SET_ANSWER);
export const deleteAllAnswers = createAction(DELETE_ALL_ANSWER);