import {createAction} from 'redux-actions';
import {TEST_VALIDATED} from './../constants/actions';

export const setTestValidated = createAction(TEST_VALIDATED);