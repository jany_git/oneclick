import {createAction} from 'redux-actions';
import {FORM_ERROR, DELETE_FORM_ERROR} from './../constants/actions';

export const showFormError = createAction(FORM_ERROR);
export const deleteFormError = createAction(DELETE_FORM_ERROR);