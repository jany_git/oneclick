import React, { Component } from 'react';
import './App.css';
import QuestionsList from './components/QuestionsList';
import FooterContainer from './containers/FooterContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <QuestionsList/>
        <FooterContainer />
      </div>
    );
  }
}

export default App;
